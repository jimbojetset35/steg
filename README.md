Simple LBS Stenography with a twist.

This software searches the source image for noisy (high entropy) areas in which to hide data. It then distributes the
data you want to hide evenly across the noisy areas of the image. This makes it virtually impossible to easily detect
if an image is hiding LSB data unless you can compare the output image with the (exact) source image (a failing of all
LSB steg programs).

Due to the pseudo-random nature of how the data is stored, a header is also stored in the image which tells the program
how much data is stored. Since this is stored in a fixed contiguous space, an image with very little entropy could give
away that there may be data in the image hence why the program also requires a keyword which is used to determine which
LSB bit of the pixel (RGBA) in which to store each bit of data. XOR encryption is also carried out against the given key
on both the header and source data prior to that data being embedded into the image. Both the header AND the data are
encrypted this way using the keyword.

There is no decoded output validation since the program doesn't know what data is stored (if any) in an image therefore
you only know you have a successful decode if the saved (extracted) data is what you were expecting (image, text etc...)

