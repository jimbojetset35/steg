import sys

import easygui
from PIL import Image
from PyQt4 import QtGui
from PyQt4.QtGui import QPixmap

import Stegano
from SteganoUi import UiDialog


class SteganoDialog(QtGui.QDialog):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.ui = UiDialog()
        self.ui.setup_ui(self)
        self.ui.pushButton_5.clicked.connect(self.getfilepath)
        self.ui.pushButton_4.clicked.connect(self.getimagepath)
        self.ui.pushButton_3.clicked.connect(self.closeform)
        self.ui.pushButton_2.clicked.connect(self.decode)
        self.ui.pushButton.clicked.connect(self.encode)
        self.ui.label_4.setPixmap(QPixmap(str('logo.jpg')))
        self.msg('Ready')
        self.show()

    def getimagepath(self):
        self.ui.lineEdit_3.setText(easygui.fileopenbox('Select an image file to open', 'Stegano'))
        self.msg('Opening ' + self.ui.lineEdit_3.text())
        try:
            self.ui.label_4.setPixmap(QPixmap(str(self.ui.lineEdit_3.text())))
        except Exception as error:
            self.msg('Failed to open ' + self.ui.lineEdit_3.text())
            self.msg(error.message)
            return

    def getfilepath(self):
        self.ui.lineEdit_2.setText(easygui.fileopenbox())

    @staticmethod
    def closeform():
        sys.exit(0)

    def encode(self):
        if len(str(self.ui.lineEdit_3.text())) == 0:
            self.msg('Please provide an image.')
            return
        if len(str(self.ui.lineEdit_2.text())) == 0:
            self.msg('Please provide a filename.')
            return
        if len(str(self.ui.lineEdit.text())) == 0:
            self.msg('Please provide a Keyword.')
            return
        self.msg('Opening ' + self.ui.lineEdit_3.text())
        try:
            image = Image.open(str(self.ui.lineEdit_3.text()))
            image = image.convert('RGBA')
            self.ui.label_4.setPixmap(QPixmap(str(self.ui.lineEdit_3.text())))
        except Exception as error:
            self.msg('Failed to open ' + self.ui.lineEdit_3.text())
            self.msg(error.message)
            return
        try:
            bitstream = Stegano.generate_bitstream_from_file(str(self.ui.lineEdit_2.text()))
        except Exception as error:
            self.msg('Failed to open ' + self.ui.lineEdit_2.text())
            self.msg(error.message)
            return
        capacity = Stegano.get_image_capacity(image)
        if len(bitstream) < capacity:
            self.msg('Writing data into image.')
            newimage = Stegano.write_bitstream_into_image(bitstream, image, str(self.ui.lineEdit.text()))
            self.msg('Saving image to ' + self.ui.lineEdit_3.text() + '.new.png')
            newimage.save(str(self.ui.lineEdit_3.text()) + '.new.png')
            self.ui.label_4.setPixmap(QPixmap(str('logo.jpg')))
            self.msg('Completed.')
        else:
            self.msg('File is too large to fit into image.')
        self.ui.label_4.setPixmap(QPixmap(str('logo.jpg')))

    def decode(self):
        if len(str(self.ui.lineEdit_3.text())) == 0:
            self.msg('Please provide an image.')
            return
        if len(str(self.ui.lineEdit_2.text())) == 0:
            self.msg('Please provide a filename.')
            return
        if len(str(self.ui.lineEdit.text())) == 0:
            self.msg('Please provide a Keyword.')
            return
        self.msg('Opening ' + self.ui.lineEdit_3.text())
        try:
            image = Image.open(str(self.ui.lineEdit_3.text()))
            self.ui.label_4.setPixmap(QPixmap(str(self.ui.lineEdit_3.text())))
        except Exception as error:
            self.msg('Failed to open ' + self.ui.lineEdit_3.text())
            self.msg(error.message)
            return
        self.msg('Extracting data from image')
        try:
            bitstream = Stegano.recover_bitstream_from_image(image, str(self.ui.lineEdit.text()))
            if len(bitstream) == 0:
                raise Exception('Zero length')
            Stegano.save_bitstream_to_file(bitstream, str(self.ui.lineEdit_2.text()))
            self.msg('Saved extracted data to ' + str(self.ui.lineEdit_2.text()))
        except Exception as error:
            self.msg('Sorry no data found!')
            self.msg(error.message)
        self.ui.label_4.setPixmap(QPixmap(str('logo.jpg')))

    def msg(self, value):
        self.ui.plainTextEdit.appendPlainText(value)


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    myapp = SteganoDialog()
    app.exec_()

