from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _from_utf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8


    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class UiDialog(object):
    def setup_ui(self, dialog):
        dialog.setObjectName(_fromUtf8("Dialog"))
        dialog.setWindowModality(QtCore.Qt.ApplicationModal)
        dialog.resize(713, 287)
        dialog.setAutoFillBackground(False)
        dialog.setModal(True)
        self.lineEdit = QtGui.QLineEdit(dialog)
        self.lineEdit.setGeometry(QtCore.QRect(75, 80, 201, 26))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.label = QtGui.QLabel(dialog)
        self.label.setGeometry(QtCore.QRect(10, 83, 58, 21))
        self.label.setObjectName(_fromUtf8("label"))
        self.lineEdit_2 = QtGui.QLineEdit(dialog)
        self.lineEdit_2.setGeometry(QtCore.QRect(75, 50, 324, 26))
        self.lineEdit_2.setObjectName(_fromUtf8("lineEdit_2"))
        self.label_2 = QtGui.QLabel(dialog)
        self.label_2.setGeometry(QtCore.QRect(10, 53, 62, 21))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_3 = QtGui.QLabel(dialog)
        self.label_3.setGeometry(QtCore.QRect(10, 23, 58, 21))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.lineEdit_3 = QtGui.QLineEdit(dialog)
        self.lineEdit_3.setGeometry(QtCore.QRect(75, 20, 324, 26))
        self.lineEdit_3.setObjectName(_fromUtf8("lineEdit_3"))
        self.pushButton = QtGui.QPushButton(dialog)
        self.pushButton.setGeometry(QtCore.QRect(140, 120, 87, 27))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton_2 = QtGui.QPushButton(dialog)
        self.pushButton_2.setGeometry(QtCore.QRect(240, 120, 87, 27))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.pushButton_3 = QtGui.QPushButton(dialog)
        self.pushButton_3.setGeometry(QtCore.QRect(340, 120, 87, 27))
        self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
        self.pushButton_4 = QtGui.QPushButton(dialog)
        self.pushButton_4.setGeometry(QtCore.QRect(400, 20, 32, 26))
        self.pushButton_4.setObjectName(_fromUtf8("pushbutton_4"))
        self.pushButton_5 = QtGui.QPushButton(dialog)
        self.pushButton_5.setGeometry(QtCore.QRect(400, 50, 32, 26))
        self.pushButton_5.setObjectName(_fromUtf8("pushbutton_5"))

        self.plainTextEdit = QtGui.QPlainTextEdit(dialog)
        self.plainTextEdit.setGeometry(QtCore.QRect(10, 164, 421, 111))
        self.plainTextEdit.setAutoFillBackground(False)
        self.plainTextEdit.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.plainTextEdit.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.plainTextEdit.setReadOnly(True)
        self.plainTextEdit.setObjectName(_fromUtf8("plainTextEdit"))
        self.label_4 = QtGui.QLabel(dialog)
        self.label_4.setGeometry(QtCore.QRect(440, 20, 261, 251))
        self.label_4.setFrameShape(QtGui.QFrame.Box)
        self.label_4.setText(_fromUtf8(""))
        self.label_4.setScaledContents(True)
        self.label_4.setObjectName(_fromUtf8("label_4"))

        self.retranslate_ui(dialog)
        QtCore.QMetaObject.connectSlotsByName(dialog)
        dialog.setTabOrder(self.lineEdit_3, self.lineEdit_2)
        dialog.setTabOrder(self.lineEdit_2, self.lineEdit)
        dialog.setTabOrder(self.lineEdit, self.pushButton)
        dialog.setTabOrder(self.pushButton, self.pushButton_2)
        dialog.setTabOrder(self.pushButton_2, self.pushButton_3)
        dialog.setTabOrder(self.pushButton_3, self.plainTextEdit)

    def retranslate_ui(self, dialog):
        dialog.setWindowTitle(_translate("Dialog", "Stegano Form", None))
        self.label.setText(_translate("Dialog", "Keyword", None))
        self.label_2.setText(_translate("Dialog", "Filename", None))
        self.label_3.setText(_translate("Dialog", "Image", None))
        self.pushButton.setToolTip(_translate("Dialog", "Encode filename data into image using keyword.", None))
        self.pushButton.setText(_translate("Dialog", "Encode", None))
        self.pushButton_2.setToolTip(
            _translate("Dialog", "Extract data from image using keyword and save to filename.", None))
        self.pushButton_2.setText(_translate("Dialog", "Decode", None))
        self.pushButton_3.setText(_translate("Dialog", "Close", None))
        self.pushButton_4.setText(_translate("Dialog", "...", None))
        self.pushButton_5.setText(_translate("Dialog", "...", None))
