from binascii import hexlify

from bitstring import BitArray


def get_image_capacity(image):
    cnt = -1
    bitmap = image.load()
    for y in range(2, image.size[1] - 1):
        for x in range(1, image.size[0] - 1):
            if _is_valid_pixel(bitmap, x, y):
                cnt += 1
    return cnt


def write_bitstream_into_image(bitstream, image, key='key'):
    cnt = 0
    cnt2 = 0
    jump = get_image_capacity(image) / len(bitstream)
    if jump > 999:
        jump = 999
    header = _generate_header(jump, len(bitstream))
    header = _xor(header, key)
    bitmap = image.load()
    l = len(key)
    for x in range(0, 40):
        bitmap = set_pixel(header, bitmap, x, 0, l, key, cnt, cnt)
        cnt += 1
    cnt = 0
    bitmap = image.load()
    bitstream = _xor(bitstream, key)
    for y in range(2, image.size[1] - 1):
        for x in range(1, image.size[0] - 1):
            if _is_valid_pixel(bitmap, x, y):
                if cnt < len(bitstream) and cnt2 % jump == 0:
                    bitmap = set_pixel(bitstream, bitmap, x, y, l, key, cnt, cnt2)
                    cnt += 1
                cnt2 += 1
    return image


def set_pixel(data, bitmap, x, y, l, key, cnt, cnt2):
    r = bitmap[x, y][0]
    g = bitmap[x, y][1]
    b = bitmap[x, y][2]
    a = bitmap[x, y][3]
    bit = int(data[cnt])
    if ord(key[cnt2 % l]) % 4 == 0:
        bitmap[x, y] = ((r & 254) + bit, g, b, a)
    if ord(key[cnt2 % l]) % 4 == 1:
        bitmap[x, y] = (r, (g & 254) + bit, b, a)
    if ord(key[cnt2 % l]) % 4 == 2:
        bitmap[x, y] = (r, g, (b & 254) + bit, a)
    if ord(key[cnt2 % l]) % 4 == 3:
        bitmap[x, y] = (r, g, b, (a & 254) + bit)
    return bitmap


def recover_bitstream_from_image(image, key='key'):
    cnt = 0
    cnt2 = 0
    bitmap = image.load()
    header = ''
    for x in range(0, 40):
        header += str(bitmap[x, 0][ord(key[x % len(key)]) % 4] % 2)
    header = _xor(header, key)
    jump = BitArray(bin=header[0:16]).int
    length = BitArray(bin=header[16:40]).int
    bitstream = ''
    for y in range(2, image.size[1] - 1):
        for x in range(1, image.size[0] - 1):
            if _is_valid_pixel(bitmap, x, y):
                if cnt < length and cnt2 % jump == 0:
                    bitstream += str(bitmap[x, y][ord(key[cnt2 % len(key)]) % 4] % 2)
                    cnt += 1
                cnt2 += 1
    return _xor(bitstream, key)


def generate_bitstream_from_file(filename):
    bitstream = ""
    with open(filename, "rb") as f:
        byte = f.read(1)
        while byte != "":
            bitstream += BitArray(hex=hexlify(byte)).bin
            byte = f.read(1)
    return bitstream


def save_bitstream_to_file(bitstream, filename):
    b = BitArray(bin=bitstream).bytes
    with open(filename, 'wb') as f:
        f.write(b)


def _is_valid_pixel(bitmap, x, y):
    cpixel = bitmap[x, y][0] & 248
    upixel = bitmap[x, y - 1][0] & 248
    dpixel = bitmap[x, y + 1][0] & 248
    lpixel = bitmap[x - 1, y][0] & 248
    rpixel = bitmap[x + 1, y][0] & 248
    return not (cpixel == upixel and cpixel == dpixel and cpixel == lpixel and cpixel == rpixel)


def _generate_header(jump, length):
    jumpstream = "{0:b}".format(jump)
    while len(jumpstream) < 16 != 0:
        jumpstream = '0' + jumpstream
    lengthstream = "{0:b}".format(length)
    while len(lengthstream) < 24 != 0:
        lengthstream = '0' + lengthstream
    header = jumpstream + lengthstream
    return header


def _xor(bitstream, key):
    keystream = _string_to_bitstream(key)
    cipherstream = ''
    while len(keystream) < len(bitstream):
        keystream += keystream
    cnt = 0
    while cnt < len(bitstream):
        cipherstream += str(int(bitstream[cnt]) ^ int(keystream[cnt]))
        cnt += 1
    return cipherstream


def _string_to_bitstream(string):
    bitstream = ''
    for s in string:
        bitstream += BitArray(hex=hexlify(s)).bin
    return bitstream
